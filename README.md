# Harvest Defense

Submission for the Ludum Dare 52 (Theme: Harvest)

## Links

  - Submission: <https://ldjam.com/events/ludum-dare/52/harvest-defense>
  - Play it online: <https://diogotito.com/static_sites/Harvest_TD>

## Development instructions

This game was made in [Godot 3.5.1](https://godotengine.org/download) (standard version).
Just clone this repo and open it in the Godot Editor.

## Credits

### Developers

  - Diogo Almiro
  - Diogo Tito
  - Pedro Granja

### Artists

  - Daniel Marques
  - Mariana Monteiro
  - Ana Patrícia Colaço
  - Tomaz Dionisio

### Attributions

This was our first time working with Godot together so, as a base, we started from the final version of the project from the video tutorial series [Tower Defense in Godot](https://www.youtube.com/playlist?list=PLZ-54sd-DMAJltIzTtZ6ZhC-9hkqYXyp6) by [Game Development Center](https://www.youtube.com/@GameDevelopmentCenter)

By proxy, we used these assets from [Kenney](https://www.kenney.nl/):
  - [Game Icons](https://kenney.nl/assets/game-icons)
  - [Kenney Fonts](https://kenney.nl/assets/kenney-fonts)
  - [UI Pack](https://kenney.nl/assets/ui-pack)