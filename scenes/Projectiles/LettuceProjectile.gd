extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var speed = 200
var projectileDamage = 10
var target = null


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if target == null || not is_instance_valid(target):
		queue_free()
		return

	var targetPos = target.global_position
	var dir = (targetPos - global_position).normalized()
	look_at(targetPos)

	var vel = dir * speed * delta
	global_position += vel



func _on_Projectile_area_entered(body):
	if body.get_parent().is_in_group("Enemy"):
		var enemy = body.get_parent()
		enemy.on_hit(projectileDamage)
		queue_free()
