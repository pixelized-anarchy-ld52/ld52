extends Node2D

var associated_order : Dictionary
var tile_location : Vector2

onready var _map_node := find_parent("Map?")

func _on_Area2D_mouse_entered():
	if (
			Input.is_action_pressed("ui_accept") and
			GameData.currentState == GameData.Modes.CANCEL_MODE
	):
		hide()
		GameData.cancel_order(associated_order.position)
		_map_node.get_node("PlantExclusion").set_cellv(tile_location, -1)
