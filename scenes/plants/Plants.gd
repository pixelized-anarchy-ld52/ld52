tool
class_name Plant
extends Node2D

signal reached_adulthood
signal killed

export(Resource) var species

#
# State
#

var _enemy_array: Array = []
var _cooling_down: bool = false

var _age : float = 0.0
onready var _hp : float = species.max_hp

# onready var _health_bar : TextureProgress = $HealthBar
onready var _baby_face : Sprite = $BabyFace
onready var _adult_face : Sprite = $AdultFace

var _baby_face_initial_scale_x = 0
var _baby_face_initial_scale_y = 0
var _adult_face_initial_scale_x = 0
var _adult_face_initial_scale_y = 0


func _get_configuration_warning() -> String:
	if species == null:
		return "Esqueceste-te de arrastar um stats.tres para o campo species!"

	if not species is PlantSpecies:
		return "O que puseste em species não é um recurso PlantSpecies!"

	if (
		not $BabyFace.visible
		or $AdultFace.visible
		or ($Sickle.visible)
		or ($HealthBar and $HealthBar.visible)
	):
		return "Visibilidade!\n" \
			 + "Quando a planta for instanciada, a Sprite BabyFace será " \
			 + "exibida e as Sprites AdultFace e Sickle deverão ser " \
			 + "escondidas. A HealthBar deverá começar invisível."

	return ""
	# return "HA HA YOU SUCK"


func _ready() -> void:
	assert(species != null, "Esqueceste-te de arrastar um stats.tres")
	assert(species is PlantSpecies, "specie tem de ser um Resource do tipo PlantSpecies!")

	var shape := $Range/CollisionShape2D as CollisionShape2D
	(shape.get_shape() as CircleShape2D).radius = species.range_radius

	_baby_face.show()
	_adult_face.hide()

	# Uncomment quando for para as plantas terem vida
	# _health_bar.max_value = _hp
	# _health_bar.value = _hp
	# _health_bar.set_as_toplevel(true)
	# _health_bar.hide()

	_baby_face_initial_scale_x = _baby_face.scale.x
	_baby_face_initial_scale_y = _baby_face.scale.y
	_adult_face_initial_scale_x = _adult_face.scale.x
	_adult_face_initial_scale_y = _adult_face.scale.y

#
# Derived state
#

func is_adult() -> bool:
	return _age >= species.time_to_adulthood


func get_selling_price() -> float:
	var x := clamp(_age / species.lifetime, 0, 1)
	return species.value_over_lifetime.interpolate_baked(x)


func will_be_adult_in(delta : float) -> bool:
	return _age + delta >= species.time_to_adulthood


func can_fire() -> bool:
	return not _cooling_down and is_adult() and species.attack_stat > 0



#
# Processing
#

func _process(delta : float) -> void:
	if Engine.editor_hint:
		return

	process_health(delta)
	process_age(delta)
	process_firing()


func process_health(delta : float) -> void:
	var x := clamp(_age / species.lifetime, 0, 1)
	_hp += delta * species.regen_over_lifetime.interpolate_baked(x)

#	_health_bar.value = _hp

	if _hp <= 0:
		emit_signal('killed')
		queue_free()  # TODO Morrer de forma mais cerimoniosa


func process_age(delta_age : float) -> void:
	# Are we becoming adult in this frame?
	if not is_adult() and will_be_adult_in(delta_age):
		become_adult()

	_age += delta_age

	var x := clamp(_age / species.lifetime, 0, 1)
	var target_scale = species.scale_over_lifetime.interpolate_baked(x)

	# we are at peak sellability and power, shake to let player know
	if target_scale >= 0.99:
		_adult_face.shake = true
	else:
		_baby_face.scale.x = target_scale * _baby_face_initial_scale_x
		_baby_face.scale.y = target_scale * _baby_face_initial_scale_y
		_adult_face.scale.x = target_scale * _adult_face_initial_scale_x
		_adult_face.scale.y = target_scale * _adult_face_initial_scale_y




func become_adult() -> void:
	emit_signal("reached_adulthood")
	_baby_face.hide()
	_adult_face.show()


#
# Firing
#

func process_firing() -> void:
	var selected_enemy := select_enemy()

	if selected_enemy != null:
		fire_at(selected_enemy)


func fire_at(enemy : Enemy) -> void:
	if not can_fire():
		return

	shoot_projectile(enemy)

	_cooling_down = true
	yield(get_tree().create_timer(species.rate_of_fire), "timeout")
	_cooling_down = false


func shoot_projectile(enemy : Enemy) -> void:
	var projectile = species.projectile_scene.instance()
	projectile.global_position = global_position
	projectile.target = enemy
	projectile.projectileDamage = species.attack_stat
	if $"../../Projectiles":
		$"../../Projectiles".add_child(projectile)


func select_enemy() -> Enemy:
	# If there aren't any enemies in range, we don't have anything to shoot at
	if _enemy_array.size() == 0:
		return null

	# Select enemy closest to base (i.e. the one that's further in its Path2D)
	var enemy_progress_array: Array = []
	for enemy in _enemy_array:
		enemy_progress_array.append(enemy.offset)
	var max_offset = enemy_progress_array.max()
	var enemy_index = enemy_progress_array.find(max_offset)
	return _enemy_array[enemy_index]


func _on_Range_body_entered(body) -> void:
	print(body)
	if body.get_parent().is_in_group("Enemy"):
		_enemy_array.append(body.get_parent())


func _on_Range_body_exited(body) -> void:
	if body.get_parent().is_in_group("Enemy"):
		_enemy_array.erase(body.get_parent())


## publico:
## Chamar esta função para danificar esta planta
func on_hit(damage: int) -> void:
	_hp -= damage
#	_health_bar.show()
#	yield(get_tree().create_timer(2.0), "timeout")
#	_health_bar.hide()
