class_name PlantSpecies
extends Resource

# um "Scriptable Object" / um "JSON" do Inspector do Godot

export(String) var display_name := "<Plant Name here>"
export(String, MULTILINE) var description := "<Plant description here>"

export(float) var price := 0.0

export(float) var max_hp := 100.0

export(float) var lifetime := 200.0
export(float) var time_to_adulthood := 25.0
export(Curve) var value_over_lifetime : Curve = null
export(Curve) var scale_over_lifetime : Curve = null
export(Curve) var regen_over_lifetime : Curve = null

export(float) var attack_stat
export(float) var defense_stat
export(float) var rate_of_fire
export(float) var range_radius

export(float) var projectile_speed
export(PackedScene) var projectile_scene
