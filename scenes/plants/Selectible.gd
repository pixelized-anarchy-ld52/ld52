extends Area2D

signal selection_toggled(selection)

export var exclusive = false

var selected = false setget set_selected

var created_order = null

func set_selected(selection):
	if selection:
		make_exclusive()
		add_to_group("selected")
	else:
		remove_from_group("selected")
	selected = selection
	if selected:
		get_parent().get_node("Sickle").visible = true
		created_order = {
			"type": "harvest",
			"position": $"../Sickle".global_position,
			"object": get_parent()
		}
		
		GameData.enqueue(created_order)
	else:
		get_parent().get_node("Sickle").visible = false
		if created_order != null:
			GameData.cancel_order(created_order.position)

	emit_signal("selection_toggled", selected)

func make_exclusive():
	if not exclusive:
		return
	get_tree().call_group("selected", "set_selected", false)

func _on_Selectible_mouse_entered():
	if Input.is_mouse_button_pressed(BUTTON_LEFT) && GameData.currentState == GameData.Modes.HARVEST_MODE:
		set_selected(true)

	if Input.is_mouse_button_pressed(BUTTON_LEFT) && GameData.currentState == GameData.Modes.CANCEL_MODE:
		set_selected(false)
