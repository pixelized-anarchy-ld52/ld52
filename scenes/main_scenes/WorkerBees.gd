extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var bees = []
var attributedOrders = []

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var offsetBetweenOrders = int(float(GameData.orderQueue.size()) / float(self.get_child_count()))

	var i = 0
	for obj in self.get_children():
		if not obj.is_in_group("WorkerBees"):
			continue

		var bee = obj

		if bee.current_order == null && GameData.orderQueue.size() > 0:
			var index = offsetBetweenOrders * i
			var target_order = GameData.orderQueue[offsetBetweenOrders * i]
			var is_available = attributedOrders.find(target_order)
			if not is_available:
				target_order = GameData.orderQueue[(index+1) % GameData.orderQueue.size()]
			if attributedOrders.find(target_order) == -1:
				attributedOrders.append(target_order)
				bee.current_order = target_order
		elif bee.current_order != null:
			if GameData.orderQueue.find(bee.current_order) == -1:
				bee.current_order = null
		i = i + 1
