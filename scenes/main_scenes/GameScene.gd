extends Node2D

signal game_finished

export(int) var amount_of_money = 60

var map_node: Node2D

var build_valid: bool = false
var build_location: Vector2
var build_tile: Vector2
var build_type: String

var current_wave: int = 0
var enemies_in_wave: int = 0
var timeToSpawnEnemies = 0

var base_health: int = 100
var build_cd: float = 0.03
var curr_cd: float

func _ready() -> void:
	map_node = $Map1

	for button in get_tree().get_nodes_in_group("build_buttons"):
		button.connect("pressed", self, "initiate_build_mode", [button.get_name()])
		button.connect("pressed", button, "set_modulate", [Color(0.75, 0.75, 0.75)], CONNECT_DEFERRED)

	var harvest_button = get_node("UI/HUD/OrderBar/HarvestMode")
	harvest_button.connect("pressed", self, "initiate_harvest_mode")
	var cancel_button = get_node("UI/HUD/OrderBar/CancelMode")
	cancel_button.connect("pressed", self, "initiate_cancel_mode")
	var spawn_button = get_node("UI/HUD/BuildBar/Bee")
	spawn_button.connect("pressed", self, "spawn_bee")

	timeToSpawnEnemies = GameData.waves_data[current_wave]["timeToSpawn"]
	curr_cd = build_cd

func _process(delta) -> void:
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		update_plant_preview()

	var harvest_button = get_node("UI/HUD/OrderBar/HarvestMode")
	harvest_button.modulate = Color(1.0,1.0,1.0)

	var cancel_button = get_node("UI/HUD/OrderBar/CancelMode")
	cancel_button.modulate = Color(1.0,1.0,1.0)

	if GameData.currentState == GameData.Modes.HARVEST_MODE:
		harvest_button.modulate = Color(0.4,0.4,0.4)

	if GameData.currentState == GameData.Modes.CANCEL_MODE:
		cancel_button.modulate = Color(0.4,0.4,0.4)

	timeToSpawnEnemies -= delta
	if timeToSpawnEnemies < 0.0 and current_wave < GameData.waves_data.size():
		start_next_wave()
		current_wave += 1
		if current_wave < GameData.waves_data.size():
			timeToSpawnEnemies = GameData.waves_data[current_wave]["timeToSpawn"]

	curr_cd -= delta

func _unhandled_input(event: InputEvent) -> void:
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		if event.is_action_released("ui_cancel"):
			cancel_build_mode()
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			verify_and_build()

	if event.is_action_released("ui_cancel"):
		GameData.currentState = GameData.Modes.NORMAL_MODE

#
# Building functions
#

func initiate_build_mode(plant_type: String) -> void:
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		cancel_build_mode()
	build_type = plant_type
	GameData.currentState = GameData.Modes.BUILD_MODE
	$UI.set_plant_preview(build_type, get_global_mouse_position(), GameData.colors.TRANSLUCENT_GREEN)
	get_tree().call_group("build_buttons", "set_modulate", Color(1, 1, 1, 1))


func update_plant_preview() -> void:
	var mouse_position: Vector2 = get_global_mouse_position()
	var plant_exclusion: TileMap = map_node.get_node("PlantExclusion")
	var current_tile: Vector2 = plant_exclusion.world_to_map(mouse_position)
	var tile_position: Vector2 = plant_exclusion.map_to_world(current_tile)

	if plant_exclusion.get_cellv(current_tile) == -1:
		$UI.update_plant_preview(tile_position, GameData.colors.TRANSLUCENT_GREEN)
		build_valid = true
		build_location = tile_position
		build_tile = current_tile
	else:
		$UI.update_plant_preview(tile_position, GameData.colors.TRANSLUCENT_RED)
		build_valid = false


func cancel_build_mode() -> void:
	get_tree().call_group("build_buttons", "set_modulate", Color.white)
	GameData.currentState = GameData.Modes.NORMAL_MODE
	build_valid = false

	# not queue_free() because it will be immediately replaced by another $PlantPreview
	$UI/PlantPreview.free()


func cost_of(plant_name : String) -> float:
	return GameData.get_plant_species(plant_name).price


func verify_and_build() -> void:
	if build_valid && amount_of_money - cost_of(build_type) >= 0 && curr_cd < 0:
		var seed_icon = load("res://scenes/plants/SeedIcon.tscn").instance()
		seed_icon.position = build_location
		map_node.get_node("Seeds").add_child(seed_icon)
		map_node.get_node("PlantExclusion").set_cellv(build_tile, 100)

		amount_of_money -= cost_of(build_type)

		var create_order = {
			"type": "build",
			"build_type": build_type,
			"position": build_location,
			"tile_position": build_tile,
			"seed_icon_object": seed_icon
		}
		GameData.enqueue(create_order)
		seed_icon.associated_order = create_order
		seed_icon.tile_location = build_tile

		curr_cd = build_cd

func initiate_harvest_mode() -> void:
	print("Harvest mode activated")
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		cancel_build_mode()
	GameData.currentState = GameData.Modes.HARVEST_MODE


func initiate_cancel_mode() -> void:
	print("Cancel mode activated")
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		cancel_build_mode()
	GameData.currentState = GameData.Modes.CANCEL_MODE

func spawn_bee() -> void:
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		cancel_build_mode()
	GameData.currentState = GameData.Modes.NORMAL_MODE
	var spawn_bee = load("res://scenes/WorkerBee.tscn").instance()
	spawn_bee.position = get_node("WorkerBees/BeeSpawner").global_position
	get_node("WorkerBees").add_child(spawn_bee)

#
# Wave functions
#

func start_next_wave() -> void:
	var wave_data = GameData.waves_data[current_wave]["enemies"]
	enemies_in_wave = wave_data.size()
	yield(get_tree().create_timer(0.2), "timeout")
	spawn_enemies(wave_data)


func spawn_enemies(wave_data: Array) -> void:
	for enemy in wave_data:
		var new_enemy: PathFollow2D = load("res://scenes/enemies/" + enemy[0] + ".tscn").instance()
		new_enemy.connect("base_damage", self, "on_base_damage")
		map_node.get_node("Path").add_child(new_enemy, true)
		yield(get_tree().create_timer(enemy[1]), "timeout")


func on_base_damage(damage: int) -> void:
	base_health -= damage
	$UI.update_health_bar(base_health)
	if base_health <= 0:
		emit_signal("game_finished", false)


func _on_Gun_mouse_entered():
	pass # Replace with function body.
