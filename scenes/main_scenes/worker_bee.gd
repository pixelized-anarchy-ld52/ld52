extends KinematicBody2D
# Worker bee

# TODO Cool class! But there's the Timer node and get_tree().crete_timer()
const Cooldown = preload("res://assets/Cooldown.gd")
const GameScene = preload("res://scenes/main_scenes/GameScene.gd")

export var speed = 1
export var workTime = 1.0

var current_order = null  # Public. Can be an order Dictionary or null

var _time_while_working := Cooldown.new(workTime)

#onready var _wander_curve := ($"../BeeWanderPosition" as Path2D).curve;
onready var _wander_pos := _pick_random_position()
onready var _wander_dir := (_wander_pos - position).normalized()

onready var _game_scene: GameScene = find_parent("GameScene")
onready var _map_node := _game_scene.find_node("Map?")
onready var _plants: Node2D = _map_node.get_node("Plants")
onready var _plant_exclusion: TileMap = _map_node.get_node("PlantExclusion")


func _process(delta) -> void:
	if current_order == null:
		if _wander_dir.dot((_wander_pos - position).normalized()) < 0:
			_wander_pos = _pick_random_position()
			_wander_dir = (_wander_pos - position).normalized()
		look_at(_wander_pos)
# warning-ignore:return_value_discarded
		move_and_slide(_wander_dir * speed)
		current_order = GameData.dequeue_by_lowest_distance(position)
		return

	var order : Dictionary = current_order
	var target_pos : Vector2 = order.position

	var dir := (target_pos - position).normalized()
	look_at(target_pos)

	if position.distance_to(target_pos) < speed * delta:
		_time_while_working.tick(delta)
		if not _time_while_working.is_ready():
			return

		# call("_%s_order" % order.type, order)
		match order.type:
			"build": _build_order(order)
			"harvest": _harvest_order(order)

		current_order = null
	else:
		var vel = dir * speed
		move_and_slide(vel)


func cancel_order(point: Vector2) -> void:
	if current_order == null:
		return

	var curr_dist = point.distance_to(current_order.position)
	if curr_dist < 1:
		current_order = null


func _build_order(order: Dictionary) -> void:
	var build_type : String = order.build_type
	var position: Vector2 = order.position
	var tile_position: Vector2 = order.tile_position
	var seed_icon_obj: Node = order.seed_icon_object

	if seed_icon_obj && is_instance_valid(seed_icon_obj):
		seed_icon_obj.queue_free()

	var new_plant := GameData.instantiate_plant(build_type)
	new_plant.position = position
	_plants.add_child(new_plant)


func _harvest_order(order: Dictionary) -> void:
	if is_instance_valid(order.object):
		var plant: Plant = order.object
		_game_scene.amount_of_money += ceil(plant.get_selling_price())

		var map_pos := _plant_exclusion.world_to_map(plant.position)
		_plant_exclusion.set_cellv(map_pos, -1)

		plant.queue_free()


func _pick_random_position() -> Vector2:
	# To use the Path2D:
	# return _wander_curve.interpolate_baked(rand_range(0, _wander_curve.get_baked_length()))

	# Get a random position inside the viewport's boundaries
	var random_pos := Vector2(
		rand_range(get_viewport_rect().position.x, get_viewport_rect().end.x),
		rand_range(get_viewport_rect().position.y, get_viewport_rect().end.y)
	)

	# "Undo" that Camera2D's zoom out to that point
	return get_canvas_transform().affine_inverse().xform(random_pos)

	# This works too!
	# return to_global(make_canvas_position_local(random_pos)))
