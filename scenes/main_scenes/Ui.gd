extends CanvasLayer
# Script for the UI node in GameScene


onready var hp_bar: TextureProgress = $HUD/InfoBar/HBox/HBoxContainer/HP
onready var hp_bar_tween: Tween = $HUD/InfoBar/HBox/HBoxContainer/HP/Tween
onready var speed_btn: Control = $HUD/InfoBar/HBox/GameControls/SpeedUp


func set_plant_preview(plant_name: String, mouse_position: Vector2, color: Color) -> void:
	var drag_plant = GameData.instantiate_plant(plant_name)
	drag_plant.set_name("DragPlant")
	drag_plant.modulate = color

	var range_texture: Sprite = Sprite.new()
	range_texture.position = Vector2(64, 64)
	var scaling: float = drag_plant.species.range_radius / 150.0
	range_texture.scale = Vector2(scaling, scaling)
	var texture: Texture = preload("res://assets/ui/range_overlay.png")
	range_texture.texture = texture
	range_texture.modulate = color

	var control = Control.new()
	control.set_scale(Vector2(0.25, 0.25))
	control.add_child(drag_plant, true)
	control.add_child(range_texture, true)
	control.rect_position = mouse_position / 4
	control.set_name("PlantPreview")
	add_child(control, true)
	move_child($PlantPreview, 0)


func update_plant_preview(new_position: Vector2, color: Color):
	$PlantPreview.rect_position = new_position / 4
	if $PlantPreview/DragPlant.modulate != color:
		$PlantPreview/DragPlant.modulate = color
		$PlantPreview/Sprite.modulate = color


func update_health_bar(base_health: int):
	hp_bar_tween.interpolate_property(hp_bar, "value", hp_bar.value, base_health, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	hp_bar_tween.start()
	if base_health >= 60:
		hp_bar.set_tint_progress(GameData.colors.GREEN)
	elif base_health >= 25:
		hp_bar.set_tint_progress(GameData.colors.ORANGE)
	else:
		hp_bar.set_tint_progress(GameData.colors.RED)


func _on_PausePlay_pressed() -> void:
	var tree: SceneTree = get_tree()
	var parent: Node = get_parent()
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		parent.cancel_build_mode()
	if tree.is_paused():
		tree.paused = false
	else:
		tree.paused = true


func _on_SpeedUp_pressed() -> void:
	var parent: Node = get_parent()
	if GameData.currentState == GameData.Modes.BUILD_MODE:
		parent.cancel_build_mode()
	if Engine.get_time_scale() == 2.0:
		Engine.set_time_scale(1.0)
		speed_btn.modulate = Color(1, 1, 1, 1)
	else:
		Engine.set_time_scale(2.0)
		speed_btn.modulate = Color(0.4, 0.7, 1.0)

		# Cool FF Button transition
		var btn_x := speed_btn.rect_position.x
		var tween := speed_btn.create_tween()
		tween.tween_property(speed_btn, "rect_position:x", btn_x + 20, .2) \
			.set_trans(Tween.TRANS_CUBIC) \
			.set_ease(Tween.EASE_IN)
		tween.tween_property(speed_btn, "rect_position:x", btn_x, .5) \
			.set_trans(Tween.TRANS_ELASTIC) \
			.set_ease(Tween.EASE_OUT)

