extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var animationSpeed = 1.0
export var scaleLowerLimit = 0.9
export var scaleUpperLimit = 1.1
export var randomFactorLowerBound = 0.0
export var randomFactorUpperBound = 0.8


var isShrinkingX = true
var isShrinkingY = false

var maxScaleX = 0
var minScaleX = 0
var maxScaleY = 0
var minScaleY = 0

var rng = RandomNumberGenerator.new()

var shake = false

# Called when the node enters the scene tree for the first time.
func _ready():
	maxScaleX = scaleUpperLimit * scale.x
	minScaleX = scaleLowerLimit * scale.x
	maxScaleY = scaleUpperLimit * scale.y
	minScaleY = scaleLowerLimit * scale.y


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not shake:
		return

	if isShrinkingX:
		scale.x -=  rng.randf_range(randomFactorLowerBound, randomFactorUpperBound) * animationSpeed * delta
		if scale.x < minScaleX:
			isShrinkingX = false
	else:
		scale.x += rng.randf_range(randomFactorLowerBound, randomFactorUpperBound) * animationSpeed * delta
		if scale.x > maxScaleX:
			isShrinkingX = true

	rng.randomize()
	if isShrinkingY:
		scale.y -= rng.randf_range(randomFactorLowerBound, randomFactorUpperBound) * animationSpeed * delta
		if scale.y < minScaleY:
			isShrinkingY = false
	else:
		scale.y += rng.randf_range(randomFactorLowerBound, randomFactorUpperBound) * animationSpeed * delta
		if scale.y > maxScaleY:
			isShrinkingY = true

