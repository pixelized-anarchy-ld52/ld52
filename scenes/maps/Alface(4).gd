extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var animationSpeed = 1.0
export var scaleLowerLimit = 0.9
export var scaleUpperLimit = 1.1


var isShrinkingX = true
var isShrinkingY = false

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rng.randomize()

	if isShrinkingX:
		scale.x -=  rng.randf_range(0.8, 1.0) * animationSpeed * delta
		if scale.x < scaleLowerLimit:
			isShrinkingX = false
	else:
		scale.x += rng.randf_range(0.8, 1.0) * animationSpeed * delta
		if scale.x > scaleUpperLimit:
			isShrinkingX = true

	rng.randomize()
	if isShrinkingY:
		scale.y -= rng.randf_range(0.8, 1.0) * animationSpeed * delta
		if scale.y < scaleLowerLimit:
			isShrinkingY = false
	else:
		scale.y += rng.randf_range(0.8, 1.0) * animationSpeed * delta
		if scale.y > scaleUpperLimit:
			isShrinkingY = true

