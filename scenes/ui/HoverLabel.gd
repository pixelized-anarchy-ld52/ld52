extends Control

class_name HoverLabel

export(String, MULTILINE) var tooltip_text;
export(Color) var tooltip_background = Color.black;
export(Color) var tooltip_color = Color.white;
export(float) var tooltip_timeout = 0;
export(float) var transition_duration := 0.6;

var panel : Panel;
var panel_y_initial : float = 0
var panel_y_final : float

func _ready():
	show_behind_parent = true

	set_margins_preset(Control.PRESET_TOP_LEFT)

	var background_style := StyleBoxFlat.new();
	background_style.bg_color = tooltip_background;

	var parent = get_parent();
	# get parent size
	var width = parent.rect_size.x
	var height = parent.rect_size.y / 2 * 1.5

	self.mouse_filter = Control.MOUSE_FILTER_IGNORE

	panel = Panel.new()
	panel.rect_size = Vector2(width * 1.15, height * 1.25)
	panel.margin_left = -width * 0.15
	panel.margin_top = -height * 1.25
	panel.margin_bottom = 0
	panel.rect_pivot_offset.x = panel.rect_size.x / 2
	panel.modulate = Color(1,1,1,0)
	panel.add_stylebox_override("panel", background_style)
	panel.mouse_filter = Control.MOUSE_FILTER_IGNORE
	add_child(panel)

	# Store Panel's y coordinate for transition
	panel_y_final = panel.rect_position.y
	panel.rect_position.y = 0

	var margin = MarginContainer.new()
	margin.set_anchors_preset(Control.PRESET_WIDE)
	add_constant_override("margin_top", 5)
	add_constant_override("margin_left", 5)
	add_constant_override("margin_bottom", 5)
	add_constant_override("margin_right", 5)
	margin.mouse_filter = Control.MOUSE_FILTER_IGNORE
	panel.add_child(margin)

	var center = Control.new()
	center.set_anchors_preset(Control.PRESET_WIDE)
	center.mouse_filter = Control.MOUSE_FILTER_IGNORE
	margin.add_child(center)

	var txt := RichTextLabel.new()
	txt.set_anchors_preset(Control.PRESET_HCENTER_WIDE)
	txt.fit_content_height = true
	txt.bbcode_enabled = true
	txt.bbcode_text = tooltip_text.replace("\\n", "\n")
	txt.add_font_override("mono_font", _make_mono_font())
	txt.add_font_override("bold_font", _make_bold_font())
	txt.add_font_override("italics_font", _make_italics_font())
	txt.modulate = tooltip_color
	txt.mouse_filter = Control.MOUSE_FILTER_IGNORE
	center.add_child(txt)
	txt.margin_top -= 0.5 * txt.get_content_height()  # Fragile

	$"../Icon".connect("mouse_entered", self, "fade_in")
	$"../Icon".connect("mouse_exited", self, "fade_out")


func fade_in():
	var duration := Engine.time_scale * transition_duration
	var tween = panel.create_tween() \
				  .set_parallel() \
				  .set_trans(Tween.TRANS_ELASTIC) \
				  .set_ease(Tween.EASE_OUT)
	tween.tween_property(panel, "modulate", Color(1,1,1,0.6), duration)
	tween.tween_property(panel, "rect_position:y", panel_y_final, duration)
	tween.tween_property(panel, "rect_scale:x", 1.0, duration)


func fade_out():
	var duration := Engine.time_scale * transition_duration
	var tween = panel.create_tween() \
				  .set_parallel() \
				  .set_trans(Tween.TRANS_QUART) \
				  .set_ease(Tween.EASE_IN)
	tween.tween_property(panel, "modulate", Color(1,1,1,0), duration)
	tween.tween_property(panel, "rect_position:y", panel_y_initial, duration)
	tween.tween_property(panel, "rect_scale:x", 0.5, duration) \
	  .set_ease(Tween.EASE_OUT_IN)


func _make_mono_font() -> DynamicFont:
	var font := DynamicFont.new()
	font.font_data = preload("res://assets/fonts/Kenney Future Narrow.ttf")
	font.size = 16
	font.extra_spacing_top = 1
	font.extra_spacing_bottom = 2
	return font


func _make_italics_font() -> DynamicFont:
	var font := _make_mono_font()
	font.size = 12
	font.extra_spacing_top = 2
	font.extra_spacing_bottom = 6
	return font


func _make_bold_font() -> DynamicFont:
	var font := _make_mono_font()
	font.size = 12
	font.extra_spacing_top = 0
	font.extra_spacing_bottom = 0
	font.outline_size = 1
	font.outline_color = Color("8bec")
	font.extra_spacing_char = 1
	return font

