extends Node

const cached_stats : Dictionary = {
	"Lettuce": preload("res://scenes/plants/Lettuce/statsLettuce.tres"),
	"Dandylion": preload("res://scenes/plants/Dandylion/statsDandy.tres"),
	"Wheat": preload("res://scenes/plants/Wheat/statsWheat.tres")
}

const cached_skeletons : Dictionary = {
	"Lettuce": preload("res://scenes/plants/Lettuce/skeleton.tscn"),
	"Dandylion": preload("res://scenes/plants/Dandylion/skeleton.tscn"),
	"Wheat": preload("res://scenes/plants/Wheat/skeleton.tscn")
}

func instantiate_plant(name : String) -> Plant:
	assert(cached_skeletons.has(name), "PLANT %s NOT IMPLEMENTED" % name)
	return cached_skeletons[name].instance()


func get_plant_species(name : String) -> PlantSpecies:
	assert(cached_stats.has(name), "PLANT %s NOT IMPLEMENTED" % name)

	return cached_stats[name]


const colors = {
	"TRANSLUCENT_RED": Color("adff4545"),
	"TRANSLUCENT_GREEN": Color("ad54ff3c"),
	"RED": Color("e11e1e"),
	"GREEN": Color("4eff15"),
	"ORANGE": Color("e1be32"),

}

const Modes = {
	NORMAL_MODE = 0,
	BUILD_MODE = 1,
	CANCEL_MODE = 2,
	HARVEST_MODE = 3
}

var currentState = Modes.NORMAL_MODE

var waves_data = []

func create_wave(var time_to_spawn, var number_crikets, var number_snail):
	var res = []
	for i in range(0, number_crikets):
		res.append(["BlueTank", 1.0])

	for i in range(0, number_snail):
		res.append(["Snail", 1.0])

	return {
		"timeToSpawn": time_to_spawn,
		"enemies": res
	}

func _ready():
	waves_data.append(create_wave(40, 1, 0))
	waves_data.append(create_wave(60, 3, 0))
	waves_data.append(create_wave(60, 5, 0))
	waves_data.append(create_wave(60, 8, 0))
	waves_data.append(create_wave(60, 5, 1))
	waves_data.append(create_wave(60, 5, 2))
	waves_data.append(create_wave(60, 5, 3))
	waves_data.append(create_wave(60, 0, 5))
	waves_data.append(create_wave(60, 10, 7))
	waves_data.append(create_wave(60, 20, 10))


onready var order_queue := []

func enqueue(order):
	order_queue.append(order)

func cancel_order(point: Vector2):
	var canceled := false
	var i = 0
	if order_queue.size() > 0:
		for order in order_queue:
			var curr_dist = (order.position - point).length()
			if (order.position - point).length() < 1:
				order_queue.pop_at(i)
				canceled = true
				break
			i+=1

	if not canceled:
		get_tree().call_group("WorkerBees", "cancel_order", point)

func dequeue_by_lowest_distance(point: Vector2):
	if order_queue.size() == 0:
		return null
	var min_dist = INF
	var idx = -1
	var i=0
	for order in order_queue:
		var curr_dist = (order.position - point).length()
		if (order.position - point).length() < min_dist:
			min_dist = curr_dist
			idx = i
		i+=1

	return order_queue.pop_at(idx)
